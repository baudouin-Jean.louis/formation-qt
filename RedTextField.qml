import QtQuick 2.0
import QtQuick.Controls 2.13

TextField {
    property bool showError: false
    background: Rectangle {
        border.color: "black"
        border.width: 2
        color: showError ? "lightred" : "lightgreen"}

}

