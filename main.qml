import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.13

Window {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Column{
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        width: 200
        spacing: 10

        RedTextField {
            id: textFieldUserName
            width: parent.width
            placeholderText: qsTr("Entrer votre nom" )
            placeholderTextColor: "#80ab45"
            showError: text == ""
        }

        RedTextField {
            id: textFieldPassword
            width: parent.width
            anchors.horizontalCenterOffset: 0
            placeholderText: qsTr("Mot de passe" )
            showError: text.length < 4
            Keys.onPressed:{
                if (event.key === Qt.Key_Return || event.key === Qt.Key_Enter){
                    console.log("key return pressed")
                }
            }
            Keys.onReturnPressed: {
                console.log("on return pressed")
            }
            Keys.onEnterPressed: {
                console.log("on enter pressed")
            }

        }

        Button {
            id: button
            width: parent.width
            enabled: textFieldPassword.text.length !== 0 && !textFieldPassword.showError
            text: qsTr("Login")
        }

    }

}
